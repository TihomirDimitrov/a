Compiler = compile source files to binary
Compilation Steps:
	- preprocess
	- compile to asm
	- asemble to object file
	- link

Linker = link object files to binary

Libraries:
	- create static library
	- link to static libraries
	- library linking order
	- create shared object (dynamic library)
	- linking and using shared objects

Compiler options:
	 - optimisations
	 - warnings
	 - define macro
