#include <vector>
#include "view.hpp"
#include "textNode.hpp"

int main(int argc, char* argv[])
{
    if(argc >= 2)
    {
        int num_nodes = atoi(argv[1]);          //The first argument is the number of nodes to create
        View view;
        for(int i=2; i<argc; ++i)
        {
            switch(i%3)
            {
            case 0:
                view.addNode(new Node(argv[i]));      //Consecutive arguments are their URLs
                break;
            case 1:
                view.addNode(new TextNode(argv[i]));
                break;
            case 2:
                view.addNode(new Node(argv[i]));
                break;
            }
        }
        
        view.show();
        
        view.render();
        
        view.hide();
        
    }
    
    return 0;
}
