#include <iostream>
#include "node.hpp"

Node::Node(char* url)
    : url_(url)
{
    std::cout << "Node with URL: " << url_ << " created\n";
}

Node::~Node()
{
    std::cout << "Node with URL: " << url_ << " destroyed\n";
}

const char* Node::getUrl()
{
    return url_;
}

void Node::changeVisibility(bool isVisible)
{
    std::cout << "Node with URL: " << url_ << " is now " << (isVisible ? "visible" : "invisible") << '\n';
}

void Node::draw()
{
    std::cout << "Drew Node with URL: " << url_ << '\n';
}

void Node::move(int X, int Y)
{
    x += X;
    y += Y;
}
