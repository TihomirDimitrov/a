#!/bin/sh
set -e

# Install location
PREFIX=/usr/local

# Compile lib
options="-std=c++17 -Ofast -flto -march=native -DNDEBUG -Wall -Wextra -Wno-parentheses -Wno-unused-result -s -pipe"
# options="-std=c++17 -g"
g++ -c -fPIC commands.cpp childProcess.cpp
# Create lib
g++ -shared -o libcppipe.so commands.o childProcess.o
rm commands.o childProcess.o

# Compile cppipe
g++ -o cppipe $options cppipe.cpp -lcppipe
chmod 755 cppipe
mv cppipe ${PREFIX}/bin

# Install headers
mkdir -p ${PREFIX}/include/cppipe
cp basicTypes.h commands.hpp childProcess.hpp ${PREFIX}/include/cppipe

# Clear old cache
rm -rf ~/.cache/cppipe ${XDG_CACHE_HOME}/cppipe
