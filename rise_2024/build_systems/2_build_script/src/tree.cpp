#include"tree.hpp"
#include"utils.hpp"
#include <iostream>
#include <fstream>
#include <assert.h>

using std::vector;
using std::string;

Tree Tree::operator+ (const Tree& other) const
{
	Tree result(*this);
	return result += other;
}

Tree& Tree::operator+=(const Tree& other)
{
	vector<person_id> newId;   //The indexes people from II tree will have in I tree
	newId.reserve(other.people_.size());

	U32 initNum = people_.size(); /* number of people at the start */
	U32 idCounter = initNum;
	for (person_id i = 0, j; i < other.people_.size(); ++i)	//find the new ids for people in II, people present in both trees get the ids from I
	{
		for (j = 0; j < initNum; ++j)
		{
			if (other.people_[i] == people_[j])
			{
				newId[i] = j;
				break;
			}
		}
		if (j >= initNum)		/* not present in I */
			newId[i] = idCounter++;
	}

	people_.reserve(idCounter);
	for (person_id i = 0; i < other.people_.size(); ++i) /* add the personal data of II to I */
	{
		if (newId[i] >= initNum) /* isnt already in I */
			people_.push_back(other.people_[i]);
	}

	relations_.reserve(idCounter);
	for (person_id i = 0; i < other.people_.size(); ++i)	 //merge the relative data
	{
		if (newId[i] >= initNum) /* isnt already in I */
		{
			relations_.push_back(other.relations_[i]);
			for (Relation& rel: relations_.back())
				rel.id = newId[rel.id];
		}
		else   //if present in both trees
		{
			person_id idIn1 = newId[i];
			relations_[idIn1].reserve(relations_[idIn1].size() + other.relations_[i].size());

			for (Relation rel: other.relations_[i])
			{
				if(newId[rel.id] >= initNum) /* not already in I */
					relations_[idIn1].push_back( {newId[rel.id], rel.type} );
			}

		}
	}

	return *this;
}

Tree& Tree::operator-= (const Tree& other)
{
	for (U32 i = 0; i < people_.size(); ++i)
	{
		for (const Person& othPers: other.people_)
		{
			if (people_[i] == othPers)
			{
				removePerson(i);
				--i;
				break;
			}
		}
	}
	return *this;
}

bool Tree::isEmpty()
{
	return people_.empty();
}

person_id Tree::last()
{
	return people_.size() - 1;
}

vector<person_id> Tree::findId(const string& name) const
{
	vector<person_id> res;
	for (person_id i = 0; i < people_.size(); ++i)
	{
		if (name == people_[i].name)
			res.push_back(i);
	}
	return res;
}

/* person_id Tree::findId(const string& name, Sex s) const */
/* { */
/* 	Person sought(name, {}, s); */
/* 	for (person_id i = 0; i < people_.size(); ++i) */
/* 	{ */
/* 		if (sought == people_[i]) */
/* 			return i; */
/* 	} */
/* 	return Nobody; */
/* } */

person_id Tree::findRootAncestor(person_id id) const
{
	for(person_id dad = findParent(id, M); dad != Nobody; dad = findParent(id, M))
	{
		id = dad;
	}

	for(person_id mom = findParent(id, F); mom != Nobody; mom = findParent(id, F))
	{
		id = mom;
	}
	return id;
}

person_id Tree::findOldestAncestor(person_id id, Sex sex) const
{
	for (const Relation& rel: relations_[id])
	{
		if (rel.type == Child && people_[ rel.id ].sex == sex)
			return findOldestAncestor(rel.id, sex);
	}
	return id;
}

person_id Tree::findCommonAncestor(const unsigned first, const unsigned second) const
{
	if(first == Nobody || second == Nobody)
		return Nobody;

	vector<U8> visited(people_.size());		//if a member is visited twice he's a common ancestor

	commonAncestorRec(first, visited);
	commonAncestorRec(second, visited);

	person_id oldest = Nobody;
	U16 oldestBday = SHRT_MAX;
	for (person_id i = 0; i < people_.size(); ++i)
	{
		if (visited[i] == 2 && people_[i].birth.year < oldestBday)
		{
			/* oldestBday = people_[i].birth.year; */ //todo figure it out
			/* oldest = i; */
			return i;
		}
	}
	return oldest;
}

person_id Tree::findParent(person_id child, Sex parentSex) const
{
	person_id parent = Nobody;
	for(const Relation& rel: relations_[child])
	{
		if (rel.type == Child && people_[rel.id].sex == parentSex)
			parent = rel.id;
	}
	return parent;
}

vector<person_id> Tree::findChildren(person_id person) const
{
	if(person == Nobody)
		return {};

	vector<person_id> children;
	for(const Relation& rel: relations_[person])
	{
		if (rel.type == Parent)
			children.push_back(rel.id);
	}
	return children;
}

vector<person_id> Tree::findChildren(person_id person, person_id exclude) const
{
	vector<person_id> children = findChildren(person);
	for(U32 i = 0; i < children.size(); ++i)
	{
		if(children[i] == exclude)
			remove(children, i);
	}
	return children;
}

person_id Tree::findSpouse(person_id person) const
{
	person_id spouse = Nobody;
	for(const Relation& rel: relations_[person])
	{
		if (rel.type == Spouse)
		{
			spouse = rel.id;
			break;
		}
	}
	return spouse;
}


RelType Tree::findRelation(person_id first, person_id second) const
{
	const auto& curRels = relations_[first];
	for(const Relation& rel: curRels)
	{
		if (rel.id == second)
			return rel.type;
	}
	return None;
}

const std::vector<Relation>& Tree::findRelations(person_id id) const
{
	return relations_[id];
}

void Tree::addPerson(const char* name, Sex sex, unsigned father, unsigned mother, EventTime birth, EventTime death)
{
	people_.emplace_back(name, birth, sex, death);

	relations_.emplace_back();	/* relations of the new person */

	person_id new_person_id = people_.size() - 1;
	addRelation(father, Parent, new_person_id);
	addRelation(mother, Parent, new_person_id);

	updateRels(new_person_id);
}

/* bool Tree::addRelation(const char* firstName, const RelType type, const char* secondName) */
/* { */

/* 	return addRelation(findId(firstName)[0], type, findId(secondName)[0]); */
/* } */

bool Tree::addRelation(const unsigned first, const RelType type, const unsigned second, bool update)
{
	if (first == Nobody || first >= people_.size() || second == Nobody || second >= people_.size() || type == None)
		return false;

	switch (type)
	{
	case Parent:
		addRelOneSide(first, Parent, second);
		addRelOneSide(second, Child, first);
		break;
	case Child:
		addRelOneSide(first, Child, second);
		addRelOneSide(second, Parent, first);
		break;
	case Sibling:
	case HalfSibling:
	case Spouse:
	case ExSpouse:
		addRelOneSide(first, type, second);
		addRelOneSide(second, type, first);
		break;
	default: assert(!"Unexpected case"); break;
	}

	if(update)
	{
		updateRels(first);
		updateRels(second);
	}
	return true;
}

void Tree::addRelOneSide(person_id first, RelType type, person_id second)
{
	unsigned i = 0;
	for (; i < relations_[first].size(); ++i)	  //Check if they are already relatives
	{
		if (relations_[first][i].id == second)
		{
			relations_[first][i].type = type;
			break;
		}
	}
	if (i >= relations_[first].size())
	{
		relations_[first].push_back({second, type});
	}
}

/* void Tree::removeRelation(const char* firstName, const char* secondName) */
/* { */
/* 	removeRelation(findId(firstName)[0], findId(secondName)[0]); */
/* } */

void Tree::removePerson(const person_id id)
{
	person_id relId;
	for (Relation rel: relations_[id])
	{
		relId = rel.id;
		for (U32 j = 0; j < relations_[relId].size(); ++j)
		{
			if (relations_[relId][j].id == id)
			{
				remove(relations_[relId], j);
				break;
			}
		}
	}
	for (unsigned i = 0; i < relations_[people_.size() - 2].size(); ++i)	//Move the last in his place and update IDs
	{
		relId = relations_[people_.size() - 1][i].id;
		for (unsigned j = 0; j < relations_[relId].size(); ++j)
		{
			if (relations_[relId][j].id == people_.size() - 1)
			{
				relations_[relId][j].id = id;
				break;
			}
		}
	}
	remove(relations_, id);
	remove(people_, id);
}

/* void Tree::removePerson(const char* personName, short year, unsigned char month, unsigned char day) */
/* { */
/* 	removePerson(findId(personName, year, month, day)); */
/* } */

void Tree::removeRelation(const unsigned first, const unsigned second)
{
	for (unsigned i = 0; i < relations_[first].size(); ++i)
	{
		if (relations_[first][i].id == second)
		{
			remove(relations_[first], i);
			break;
		}
	}
	for (unsigned i = 0; i < relations_[second].size(); ++i)
	{
		if (relations_[second][i].id == first)
		{
			remove(relations_[second], i);
			break;
		}
	}
}

void Tree::focusPerson(person_id id)
{
	focused_ = id;
}

person_id Tree::focused() const
{
	return focused_;
}

void Tree::updateRels(person_id id)
{
	person_id dad = findParent(id, M);
	person_id mom = findParent(id, F);

	auto dad_children = findChildren(dad, id);
	auto mom_children = findChildren(mom, id);

	for(U32 i = 0; i < dad_children.size(); ++i)
	{
		for(U32 j = 0; j < mom_children.size(); ++j)
		{
			/* full sibling found, add and continue */
		    if (dad_children[i] == mom_children[j])
			{
				addRelation(id, Sibling, dad_children[i], false);
				remove(dad_children, i);
				remove(mom_children, j);
				--i;
				break;
			}
		}
	}

	for(U32 i = 0; i < dad_children.size(); ++i)
		addRelation(id, HalfSibling, dad_children[i], false);
	for(U32 i = 0; i < mom_children.size(); ++i)
		addRelation(id, HalfSibling, mom_children[i], false);

}


void Tree::draw(const person_id selected, const U8 zoom, const I16 offsetY, const I16 offsetX)
{
	selected_ = selected;
	zoom_ = zoom;

	erase(); //Clear the screen
	wnoutrefresh(stdscr);

	drawLineWithWives(focused_, offsetY, offsetX);

	doupdate();
}

void Tree::printMember(const unsigned id)const // todo: rename
{
	people_[id].displayInfo();
}


void Tree::commonAncestorRec(person_id id, vector<U8> &visited) const
{
	++visited[id];

	auto& curRels = relations_[id];
	for (const Relation& rel: curRels)
	{
		if (rel.type == Child)
			commonAncestorRec(rel.id, visited);
	}
}

// return the height of the space occupied by the link
static U8 linkToFirstBorn(I16 startY, I16 startX, bool moreSiblings)
{
	if (willBeVisible(startY, startX, 3, 1))
	{
		AutoWin pad( newpad(3, 1) );

		waddch(pad, ACS_VLINE);
		mvwaddch(pad, 1, 0,
				 moreSiblings ? ACS_LTEE : ACS_VLINE);
		mvwaddch(pad, 2, 0, ACS_VLINE);

		drawpad(pad, startY, startX);
	}
	return 3;
}

static U8 linkToChild(I16 startY, I16 startX, I16 endX, bool moreSiblings)
{
	assert(endX >= startX);
	const I16 len = endX - startX;
	if (willBeVisible(startY, startX, 2, len))
	{
		AutoWin pad( newpad(2, len) );

		for(U16 i = 0; i < len-1; ++i)
			waddch(pad, ACS_HLINE);
		/* whline(pad, '_', len-1); */
		waddch(pad, moreSiblings ? ACS_TTEE : ACS_URCORNER);

		mvwaddch(pad, 1, len-1, ACS_VLINE);

		drawpad(pad, startY, startX);
	}
	return 2;
}


/* I16 Tree::drawLine(person_id id, I16 drawY, I16 drawX) const */
/* { */
/* 	people_[id].draw(drawY, drawX, true, false, true); */

/* 	vector<person_id> children; */
/* 	for(U32 i=0; i<relations_[id].size(); ++i) */
/* 	{ */
/* 		if (relations_[id][i].type == RelType::Parent) */
/* 			children.push_back(relations_[id][i].id); */
/* 	} */

/* 	if (!children.empty()) */
/* 	{ */
/* 		auto prevX = drawX; */
/* 		drawY += BOX_HEIGHT; */

/* 		auto lnHt = linkToFirstBorn(drawY, drawX); /\* Link height *\/ */
/* 		drawX = drawLine(children[0], drawY + lnHt, drawX); */

/* 		for(size_t j = 1; j < children.size(); ++j) */
/* 		{ */
/* 			lnHt = linkToChild(drawY+1, prevX+1, drawX+1); */
/* 			prevX = drawX; */
/* 			drawX = drawLine(children[j], drawY + lnHt, drawX); */
/* 		} */
/* 		return drawX; */
/* 	} */

/* 	return drawX + people_[id].boxWidth() + dist_between_; */
/* } */

I16 Tree::drawLineWithWives(const person_id id, I16 drawY, const I16 drawX) const
{
	const Person& person = people_[id];
	const auto& rels = relations_[id];

	person_id spouse = Nobody;
	vector<person_id> children;
	for(const auto& rel: rels)
	{
		if (rel.type == RelType::Parent)
			children.push_back(rel.id);

		else if (rel.type == RelType::Spouse && spouse == Nobody)
			spouse = rel.id;
	}
	const size_t numKids = children.size();
	U16 person_w = person.draw(drawY, drawX,
							   id == selected_, spouse!=Nobody, true, numKids!=0, zoom_==1);

	U16 spouse_w = 0;
	if (spouse != Nobody)
	{
		U16 spouse_box = people_[spouse].draw(drawY, drawX + person_w + spouse_dist_,
											 spouse == selected_, false, false, false, zoom_==1);
		spouse_w = spouse_dist_ + spouse_box;
	}

	auto childX = drawX;
	if (numKids!=0)
	{
		auto prevX = childX;
		drawY += BOX_HEIGHT;

		auto lnHt = linkToFirstBorn(drawY, childX, numKids > 1); /* Link height */
		childX = drawLineWithWives(children[0], drawY + lnHt, childX);

		for(size_t j = 1; j < numKids; ++j)
		{
			/* +1 because LTC starts lower then LTFB */
			lnHt = linkToChild(drawY+1, prevX+1, childX+1, j < numKids-1) + 1;

			prevX = childX;
			childX = drawLineWithWives(children[j], drawY + lnHt, childX);
		}
	}

	return max(drawX + person_w + dist_between_ + spouse_w,
			   childX);
}
