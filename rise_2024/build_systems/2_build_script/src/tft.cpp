#include "tree.hpp"
#include "utils.hpp"
#include "parser.hpp"
#include "ui.hpp"
#include <ncursesw/ncurses.h>
#include <iostream>
#include <string.h>

using std::cout;

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		cout << "Usage: tft FILES...\n";
		return 1;
	}
	if(!strcmp(argv[1], "--help"))
	{
		cout << "Usage: tft FILES...\n"
		     << "Display the combined family tree from the FILES\n\n"
		     << VISUALIZATION_HELP_STR;
		return 0;

	}

	Tree tree( parseTftFile(argv[1]) );

	for(int i = 2; i < argc; ++i)
	{
		tree += parseTftFile(argv[i]);
	}

	init_ncurses();
	displayTree(tree);

	return 0;
}
