#pragma once

#include <climits>
#include <vector>
#include "person.hpp"

using person_id = U32;
enum: person_id {
	Nobody = UINT32_MAX
};

enum RelType : U8
{
	None = 0,
	Parent,
	Child,
	Sibling,
	HalfSibling,
	Spouse,
	ExSpouse,

	Other
};

struct Relation
{
	person_id id;
	RelType type;
};

class Tree
{
public:
	Tree() = default;
	template <class Str>
	explicit Tree(Str&& Name) : treeName_(std::forward<Str>(Name)) {}

	Tree operator+ (const Tree& other) const;
	Tree operator- (const Tree& other) const; /* todo */
	Tree& operator+= (const Tree& other);
	Tree& operator-= (const Tree& other);

	bool isEmpty();
	person_id last();

	std::vector<person_id> findId(const std::string& name) const;
	person_id findId(const std::string& name, Sex) const;
	person_id findRootAncestor(person_id) const;
	person_id findOldestAncestor(person_id, Sex) const;//

	//oldest common ancestor ID, a person is considerd an ancestor of himself
	person_id findCommonAncestor(person_id first, person_id second) const; /* needs to be fixed, never return nobody */
	RelType   findRelation(person_id first, person_id second) const;
	const std::vector<Relation>& findRelations(person_id) const;
	person_id findParent(person_id child, Sex parentSex) const;
	person_id findSpouse(person_id person) const;
	std::vector<person_id> findChildren(person_id person) const;
	std::vector<person_id> findChildren(person_id person, person_id exclude) const;

	void addPerson(const char* name, Sex sex, person_id father=Nobody, person_id mother=Nobody, EventTime birth={}, EventTime death={});
	bool addRelation(const char* firstName, RelType, const char* secondName);   //Adds a new relation or edits an old one. 0 - failure 1 - success
	bool addRelation(person_id first, RelType, person_id second, bool update=true);   //0 - failure 1 - success

	void removePerson(person_id id);   //Removes a member and the last one takes his ID
	/* void removePerson(const char*, short year = 0, const unsigned char month = 0, const unsigned char day = 0); */
	void removeRelation(person_id first, person_id second);
	void removeRelation(const char* firstName, const char* secondName);

	/* draw the tree based on this person */
	void focusPerson(person_id);
	person_id focused() const;

	/* void printRel(person_id id)const; //Prints out close relatives */
	void printMember(person_id)const;
	void draw(person_id selected, U8 zoom, I16 offsetY, I16 offsetX);

private:
	// recursion to find common ancestor
	void commonAncestorRec(person_id, std::vector<U8> &visited)const;

	void addRelOneSide(person_id first, RelType, person_id second);
	/* update a person's relations after adding a relation
	   (example adds siblings after parents are added) */
	void updateRels(person_id);

	/* draw a line of people starting from person with id
	  return the farthest X that will be drawn */
	I16 drawLineWithWives(person_id, I16 drawY, I16 drawX) const;

	std::string treeName_;
	std::vector<Person> people_;  //The data for each member
	std::vector<std::vector<Relation>> relations_;  //The relatives of each member

	// 0 is the first person in the tree
	person_id focused_ = 0;			/* the tree was draw based on this person */
	person_id selected_ = 0;
	U8 zoom_ = 0;

	// todo: figure out what to do with these constants
	const U8 dist_between_ = 1;		/* distance between people */
	const U8 spouse_dist_  = 0;		/* distance between spouses */
};
